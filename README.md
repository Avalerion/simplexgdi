# Simplex #
![Simplex.png](https://bitbucket.org/repo/EMrjR5/images/4164188611-Simplex.png)

### Simplex (Core) ###
* Math Functions
* Vector2
* Vector3
* Matrix4
* Quaternion

### SimplexGDI ###
* GDICore
* GameObject
* Mesh