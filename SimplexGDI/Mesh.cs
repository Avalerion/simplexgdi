﻿using Simplex;
using System.Drawing;
/// <summary>
/// 
/// </summary>
public class Mesh : GameObject {
	#region DATA
	public Vector3[] vertices;
	public int[] indices;
	public Type type;
	public Color color;
	#endregion DATA
	public static readonly Mesh quad = new Mesh(
		new Vector3[] {
			new Vector3(-0.5, -0.5),
			new Vector3(-0.5, 0.5),
			new Vector3(0.5, 0.5),
			new Vector3(0.5, -0.5)
		},
		new int[] {0, 1, 2, 0, 2, 3},
		Type.Poly,
		Color.White
	);
	public static readonly Mesh pyramid = new Mesh(
		new Vector3[] {
			new Vector3(-0.75, -0.5, -0.75),
			new Vector3(0.75, -0.5, -0.75),
			new Vector3(-0.75, -0.5, 0.75),
			new Vector3(0.75, -0.5, 0.75),
			new Vector3(0.0, 0.75, 0.0)
		},
		new int[] { 2,0,1,1,3,2,0,4,2,2,4,3,3,4,1,1,4,0 },
		Type.Poly,
		Color.Yellow
	);
	
	public static readonly Mesh cube = new Mesh(
		new Vector3[] {
			new Vector3(-0.5, -0.5, -0.5),
			new Vector3(-0.5, 0.5, -0.5),
			new Vector3(0.5, 0.5, -0.5),
			new Vector3(0.5, -0.5, -0.5),
			new Vector3(-0.5, -0.5, 0.5),
			new Vector3(-0.5, 0.5, 0.5),
			new Vector3(0.5, 0.5, 0.5),
			new Vector3(0.5, -0.5, 0.5)
		},
		new int[] { 0,1,2,0,2,3,7,4,5,7,5,6,3,7,2,2,7,6,0,4,1,4,1,5,2,1,5,2,5,6,3,4,7,0,4,3 },
		Type.Poly,
		Color.Snow
	);
	public static readonly Mesh cubel = new Mesh(
		new Vector3[] {
			new Vector3(-0.5, -0.5, -0.5),
			new Vector3(-0.5, 0.5, -0.5),
			new Vector3(0.5, 0.5, -0.5),
			new Vector3(0.5, -0.5, -0.5),
			new Vector3(-0.5, -0.5, 0.5),
			new Vector3(-0.5, 0.5, 0.5),
			new Vector3(0.5, 0.5, 0.5),
			new Vector3(0.5, -0.5, 0.5)
		},
		new int[] { 
			0,1,1,2,2,3,3,0,
			4,5,5,6,6,7,7,4,
			1,5,2,6,0,4,3,7
		},
		Type.Line,
		Color.Snow
	);
	public Mesh(Vector3[] vertices, int[] indices, Type type, Color color) {
		this.vertices = vertices;
		this.indices = indices;
		this.type = type;
		this.color = color;
	}
	public Vector3 this[int index] {
		get { return vertices[indices[index]]; }
	}
	public enum Type { Line, Poly};
}