﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Simplex;
/// <summary>
/// 
/// </summary>
public class GDICore : Form {
	#region DATA
	private Stopwatch stopwatch = new Stopwatch();
	protected double deltaTime {get; private set;}
	protected double time {get; private set;}
	protected int width {get; private set;}
	protected int height {get; private set;}
	protected int hWidth {get; private set;}
	protected int hHeight {get; private set;}
	protected Graphics graphics {get; private set;}
	protected Matrix4 camera = Matrix4.identity;
	#endregion DATA
	protected GDICore() {
		DoubleBuffered = true;
	}
	public void Start() {
		Show();
		SetScreen();
		OnResize();
		OnLoad();
		while (!IsDisposed) {
			Application.DoEvents();
			Refresh();
			Thread.Sleep(2);
		}
	}
	protected override void OnPaint(PaintEventArgs e) {
		deltaTime = stopwatch.ElapsedMilliseconds * 0.001;
		time += deltaTime;
		stopwatch.Reset();
		stopwatch.Start();
		graphics = e.Graphics;
		OnRender();
		base.OnPaint(e);
	}
	private void SetScreen() {
		width = ClientSize.Width;
		height = ClientSize.Height;
		hWidth = width / 2;
		hHeight = height / 2;
	}
	protected override void OnResize(EventArgs e) {
		SetScreen();
		OnResize();
		base.OnResize(e);
	}
	protected override void OnKeyPress (KeyPressEventArgs e) {
		
	}

	protected virtual void OnRender() {

	}
	protected virtual void OnLoad() {
		
	}
	protected virtual void OnResize() {
		
	}
	private Point Convert(Vector3 v) {
		double num = v.z == 0 ? 1 : 1.0 / v.z;
		return new Point((int)(v.x * hWidth * num) + hWidth, (int)(v.y * hHeight * num) + hHeight);
	}
	protected void DrawCircle(Vector3 p, int size, Color color) {
		//size *= height;
		Point pp = Convert(p);
		int num = size / 2;
		pp.X -= num;
		pp.Y -= num;
		Rectangle rect = new Rectangle(pp, new Size(size, size));
		using(Brush brush = new SolidBrush(color)) graphics.FillEllipse(brush, rect);
	}
	protected void DrawLine(Vector3 a, Vector3 b, Color color) {
		using(Pen pen = new Pen(color)) graphics.DrawLine(pen, Convert(a), Convert(b));
	}
	protected void DrawPoly(Vector3 a, Vector3 b, Vector3 c, Color color) {
		using(Brush brush = new SolidBrush(color)) graphics.FillPolygon(brush,new Point[]{Convert(a), Convert(b), Convert(c)});
	}
	protected void DrawPolyLine(Vector3 a, Vector3 b, Vector3 c, Color color) {
		using(Pen pen = new Pen(color)) graphics.DrawPolygon(pen,new Point[]{Convert(a), Convert(b), Convert(c)});
	}
	protected void DrawMesh(Mesh mesh, bool line = false) {
		Matrix4 temp = mesh.matrix * camera;
		switch (mesh.type) {
			case Mesh.Type.Line:
				for (int i = 0; i < mesh.indices.Length - 1; i += 2)
					DrawLine(temp * mesh[i],temp *  mesh[i+1], mesh.color);
				break;
			case Mesh.Type.Poly:
				for (int i = 0; i < mesh.indices.Length - 2; i += 3) {
					if(line) DrawPolyLine(temp * mesh[i], temp * mesh[i+1], temp * mesh[i+2], mesh.color);
					else DrawPoly(temp * mesh[i], temp * mesh[i+1], temp * mesh[i+2], mesh.color);
				}
				break;
		}
	}
}