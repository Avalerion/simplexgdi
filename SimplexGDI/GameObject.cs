﻿using Simplex;
/// <summary>
/// 
/// </summary>
public class GameObject {
	private Matrix4 _matrix = Matrix4.identity;
	private Quaternion _rotation = Quaternion.identity;
	private Vector3 _position = Vector3.zero;
	private Vector3 _scale = Vector3.one;
	private bool change;
	public Matrix4 matrix {
		get {
			if(change) { change = false; _matrix = new Matrix4(_rotation, _position, _scale); }
			return _matrix;
		}
	}
	public Quaternion rotation {
		get { return _rotation; }
		set { change = true; _rotation = value; }
		
	}
	public Vector3 position {
		get { return _position; }
		set { change = true; _position = value; }
	}
	public Vector3 scale {
		get { return _scale; }
		set { change = true; _scale = value; }
	}
}