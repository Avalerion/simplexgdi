﻿using System;
using System.Drawing;

using System.Windows.Forms;
using Simplex;
/// <summary>
/// 
/// </summary>
public class Example : GDICore {
	bool mode;
	Mesh mesh = Mesh.cube;
	Mesh mesh2 = Mesh.pyramid;
	Mesh mesh3 = Mesh.cubel;
	Font font = new Font("Courier New",12,FontStyle.Bold,GraphicsUnit.Pixel);
	Brush white = new SolidBrush(Color.White);
	private static void Main(string[] args) {
		GDICore example = new Example();
		example.Start();
	}
	protected override void OnLoad() {
		Size = new Size(640,480);
		FormBorderStyle = FormBorderStyle.FixedSingle;
		MaximizeBox = false;
		Text = "Example";
		mesh.position = Vector3.forward * 5;
		mesh3.position = mesh.position;
		mesh2.scale = Vector3.one * 0.2;
	}
	protected override void OnKeyPress (KeyPressEventArgs e) {
		if(e.KeyChar =='c' || e.KeyChar == 'C') {
			mode =!mode;
			OnResize();
		}
	}
	protected override void OnRender() {
		graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality	;
		graphics.Clear(Color.SteelBlue);
	
		for (double i = -1.25; i <= 1.25; i+=0.5) {
			mesh2.position = new Vector3(i, MathS.Sin(time*-i), 5);
			mesh2.rotation = Quaternion.FromEuler(0,-time * i ,-time * 1.5 );
			//mesh2.color = Color.Gold;
			mesh2.color = Color.FromArgb((int)(MathS.PingPong(time*0.2 ,1)*255),(int)(MathS.Abs(MathS.Sin(mesh2.position.x + time*0.2))%1*255),(int)(MathS.Abs(MathS.Cos(mesh2.position.y+ time*0.1))%1*255));
			DrawMesh(mesh2);
			mesh2.color = Color.Peru;
			DrawMesh(mesh2, true);
		}
		mesh.rotation = Quaternion.FromEuler(time,MathS.Sin(MathS.PingPong(time,Math.PI)), MathS.Cos(MathS.PingPong(time*1.33,Math.PI)));
		mesh.color = Color.FromArgb(64,(int)MathS.PingPong(time*time,150)+100,(int)MathS.PingPong(time * 33,150)+100,(int)MathS.PingPong(time*50,150)+100);
		DrawMesh(mesh);
		mesh3.rotation = mesh.rotation;
		DrawMesh(mesh3,true);		
		DrawAxis(mesh.matrix);
		
		string text = string.Format("Wellcome to Simplex Example!\nPress 'C' to change camera mode \nCurent mode: {1} \nDeltaTime: {0}",deltaTime,   mode ? "Orthographic" : "Perspective");
		graphics.DrawString(text,font,white,1,1);
	}
	private void DrawAxis(Matrix4 rotation) {
		Matrix4 temp = rotation * camera;
		Vector3 a = temp * (Vector3.up *0.25);
		Vector3 b = temp * (Vector3.right *0.25);
		Vector3 c = temp * (Vector3.forward *0.25);
		DrawLine(Vector3.zero, a, Color.Green);
		DrawLine(Vector3.zero, b, Color.Red);
		DrawLine(Vector3.zero, c, Color.Blue);
		DrawCircle(Vector3.zero, 5, Color.Snow);
		DrawCircle(a, 5, Color.Green);
		DrawCircle(b, 5, Color.Red);
		DrawCircle(c, 5, Color.Blue);
	}
	protected override void OnResize() {
		double aspect = width/(double)height;
		if(mode)camera = Matrix4.Orthographic(2.5 * aspect,2.5,0,100);
		else camera = Matrix4.Perspective(MathS.Deg2Rad * 30, aspect, 0.1, 100);
	}
}