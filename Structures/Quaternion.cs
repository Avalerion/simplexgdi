﻿namespace Simplex {
	public struct Quaternion {
		#region DATA
		private double[] data;
		#endregion DATA
		#region FIELDS
		public static readonly Quaternion identity = new Quaternion(0.0, 0.0, 0.0, 1.0);
		#endregion FIELDS
		#region ACCESSORS
		public double x {
			get { return this.data[0]; }
		}
		public double y {
			get { return this.data[1]; }
		}
		public double z {
			get { return this.data[2]; }
		}
		public double w {
			get { return this.data[3]; }
		}
		#endregion ACCESSORS
		#region CONSTRUCTORS
		public Quaternion(double x, double y, double z, double w) {
			this.data = new double[] { x, y, z, w };
		}
		#endregion CONSTRUCTORS
		#region FUNCTIONS
		public Vector3 axis {
			get {return Axis(this);}
		}
		public double magnitude {
			get { return Magnitude(this); }
		}
		public double sqrMagnitude {
			get { return SqrMagnitude(this); }
		}
		public Quaternion normalized {
			get { return Normalize(this); }
		}
		public double yaw {
			get {
				return Yaw(this);
			}
		}
		public double pitch {
			get {
				return Pitch(this);
			}
		}
		public double roll {
			get {
				return Roll(this);
			}
		}
		public static double Yaw(Quaternion q) {
			return MathS.Asin(-2.0 * (q.data[0] * q.data[2] - q.data[3] * q.data[1]));
		}
		public static double Pitch(Quaternion q) {
			return MathS.Atan2(2.0 * (q.data[1] * q.data[2] + q.data[3] * q.data[0]), q.data[3] * q.data[3] - q.data[0] * q.data[0] - q.data[1] * q.data[1] + q.data[2] * q.data[2]);
		}
		public static double Roll(Quaternion q) {
			return MathS.Atan2(2.0 * (q.data[0] * q.data[1] + q.data[3] * q.data[2]), q.data[3] * q.data[3] + q.data[0] * q.data[0] - q.data[1] * q.data[1] - q.data[2] * q.data[2]);
		}
		public static double Magnitude(Quaternion q) {
			return MathS.Sqrt(SqrMagnitude(q));
		}
		public static double SqrMagnitude(Quaternion q) {
			return q.data[0] * q.data[0] + q.data[1] * q.data[1] + q.data[2] * q.data[2] + q.data[3] * q.data[3];
		}
		public static double Dot(Quaternion a, Quaternion b) {
			return a.data[0] * b.data[0] + a.data[1] * b.data[1] + a.data[2] * b.data[2] + a.data[3] * b.data[3];
		}
		public static Quaternion Slerp(Quaternion a, Quaternion b, double t) {
			double num = Dot(a, b);
			if (MathS.Approximately(num, 1.0)) {
				return (b * t) + (a * (1.0 - t));
			}
			double num2 = MathS.Acos(num);
			double num3 = 1.0 / MathS.Sin(num2);
			double num4 = num3 * MathS.Sin(num2 - t * num2);
			double num5 = num3 * MathS.Sin(t * num2);
			return (b * num5) + (a * num4);
		}
		public static Quaternion Normalize(Quaternion q) {
			double num = 1.0 / q.magnitude;
			return new Quaternion(q.data[0] * num, q.data[1] * num, q.data[2] * num, q.data[3] * num);
		}
		public static Quaternion Inverse(Quaternion q) {
			return new Quaternion(-q.data[0], -q.data[1], -q.data[2], q.data[3]);
		}
		public static double Angle(Quaternion a, Quaternion b) {
			return MathS.Acos(MathS.Abs(Dot(a, b))) * 2.0;
		}
		public static Vector3 Axis(Quaternion q) {
			return new Vector3(
				q.data[1] * q.data[0] - q.data[2] * q.data[3] - q.data[3] * q.data[2] + q.data[0] * q.data[1],
				q.data[1] * q.data[1] + q.data[3] * q.data[3] - q.data[0] * q.data[0] - q.data[2] * q.data[2],
				q.data[1] * q.data[2] + q.data[0] * q.data[3] + q.data[2] * q.data[1] + q.data[3] * q.data[0]
			);
		}
		public static Vector2 AxisFlat(Quaternion q) {
			return new Vector2(-q.data[2] * q.data[3] - q.data[3] * q.data[2], q.data[3] * q.data[3] - q.data[2] * q.data[2]);
		}
		public static double AngleAroundAxis(Quaternion q) {
			return MathS.Acos(q.data[3]) * 2.0;
		}
		public static Quaternion FromAngleAxis(double angle, Vector3 axis) {
			double num = angle * 0.5;
			double num2 = MathS.Sin(num);
			return new Quaternion(
				axis.x * num2,
				axis.y * num2,
				axis.z * num2,
				MathS.Cos(num)
			);
		}
		public static Quaternion FromEuler(double angle) {
			double num = angle * 0.5;
			return new Quaternion(0, 0, MathS.Sin(num), MathS.Cos(num));
		}
		public static Quaternion FromEuler(double x, double y, double z) {
			double num = x * 0.5;
			double num1 = y * 0.5;
			double num2 = z * 0.5;
			double num3 = MathS.Sin(num);
			double num4 = MathS.Cos(num);
			double num5 = MathS.Sin(num1);
			double num6 = MathS.Cos(num1);
			double num7 = MathS.Sin(num2);
			double num8 = MathS.Cos(num2);
			return new Quaternion(
				num6 * num3 * num8 + num5 * num4 * num7,
				num5 * num4 * num8 - num6 * num3 * num7,
				num6 * num4 * num7 - num5 * num3 * num8,
				num6 * num4 * num8 + num5 * num3 * num7
			);
		}
		#endregion FUNCTIONS
		#region OPERATORS
		public static Quaternion operator *(Quaternion q, double d) {
			return new Quaternion(q.data[0] * d, q.data[1] * d, q.data[2] * d, q.data[3] * d);
		}
		public static Quaternion operator *(double d, Quaternion q) {
			return new Quaternion(q.data[0] * d, q.data[1] * d, q.data[2] * d, q.data[3] * d);
		}
		public static Quaternion operator /(Quaternion q, double d) {
			double num = 1.0 / d;
			return new Quaternion(q.data[0] * num, q.data[1] * num, q.data[2] * num, q.data[3] * num);
		}
		public static Vector3 operator *(Quaternion q, Vector3 v) {
			double num = q.data[3] * v.x + q.data[1] * v.z - q.data[2] * v.y;
			double num2 = q.data[3] * v.y + q.data[2] * v.x - q.data[0] * v.z;
			double num3 = q.data[3] * v.z + q.data[0] * v.y - q.data[1] * v.x;
			double num4 = q.data[0] * v.x + q.data[1] * v.y + q.data[2] * v.z;
			return new Vector3(
				num4 * q.data[0] + num * q.data[3] - num2 * q.data[2] + num3 * q.data[1],
				num4 * q.data[1] + num2 * q.data[3] - num3 * q.data[0] + num * q.data[2],
				num4 * q.data[2] + num3 * q.data[3] - num * q.data[1] + num2 * q.data[0]
			);
		}
		public static Vector2 operator *(Quaternion q, Vector2 v) {
			double num = -q.data[2] * v.y;
			double num2 = q.data[3] * v.y + q.data[2] * v.x;
			return new Vector2(num * q.data[3] - num2 * q.data[2], num2 * q.data[3] + num * q.data[2]);
		}
		public static Quaternion operator *(Quaternion a, Quaternion b) {
			return new Quaternion(
				a.data[3] * b.data[0] + a.data[0] * b.data[3] + a.data[1] * b.data[2] - a.data[2] * b.data[1],
				a.data[3] * b.data[1] + a.data[1] * b.data[3] + a.data[2] * b.data[0] - a.data[0] * b.data[2],
				a.data[3] * b.data[2] + a.data[2] * b.data[3] + a.data[0] * b.data[1] - a.data[1] * b.data[0],
				a.data[3] * b.data[3] - a.data[0] * b.data[0] - a.data[1] * b.data[1] - a.data[2] * b.data[2]
			);
		}
		public static Quaternion operator +(Quaternion a, Quaternion b) {
			return new Quaternion(a.data[0] + b.data[0], a.data[1] + b.data[1], a.data[2] + b.data[2], a.data[3] + b.data[3]);
		}
		public static Quaternion operator -(Quaternion a, Quaternion b) {
			return new Quaternion(a.data[0] - b.data[0], a.data[1] - b.data[1], a.data[2] - b.data[2], a.data[3] + b.data[3]);
		}
		#endregion OPERATORS
		#region IMPLICIT
		public static implicit operator double[](Quaternion q) {
			return q.data;
		}
		#endregion IMPLICIT
		public override string ToString() {
			return string.Format("{0:F1}, {1:F1}, {2:F1}, {3:F1}", this.data[0], this.data[1], this.data[2], this.data[3]);
		}
	}
}