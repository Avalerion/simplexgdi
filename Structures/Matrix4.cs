﻿namespace Simplex {
	public struct Matrix4 {
		#region DATA
		private double[] data;
		#endregion DATA
		#region FIELDS
		public static readonly Matrix4 identity = new Matrix4(new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0 });
		#endregion FIELDS
		#region ACCESSORS
		public double this[int index] {
			get { return this.data[index]; }
		}
		#endregion ACCESSORS
		#region CONSTRUCTORS
		private Matrix4(double[] data) {
			this.data = data;
		}
		public Matrix4(Quaternion rotation) {
			this = Rotate(rotation);
		}
		public Matrix4(Quaternion rotation, Vector3 position) {
			this = Rotate(rotation);
			Translate(this, position);
		}
		public Matrix4(Quaternion rotation, Vector3 position, Vector3 scale) {
			this = Rotate(rotation);
			Translate(this, position);
			Scale(this, scale);
		}
		public Matrix4(double angle, Vector3 axis) {
			this = Rotate(angle, axis);
		}
		public Matrix4(double angle, Vector3 axis, Vector3 position) {
			this = Rotate(angle, axis);
			Translate(this, position);
		}
		public Matrix4(double angle, Vector3 axis, Vector3 position, Vector3 scale) {
			this = Rotate(angle, axis);
			Translate(this, position);
			Scale(this, scale);
		}
		#endregion CONSTRUCTORS
		#region FUNCTIONS
		private static void Scale(Matrix4 m, Vector3 scale) {
			m.data[0] *= scale.x;
			m.data[1] *= scale.x;
			m.data[2] *= scale.x;
			m.data[4] *= scale.y;
			m.data[5] *= scale.y;
			m.data[6] *= scale.y;
			m.data[8] *= scale.z;
			m.data[9] *= scale.z;
			m.data[10] *= scale.z;
		}
		private static void Translate(Matrix4 m, Vector3 position) {
			m.data[12] += position.x;
			m.data[13] += position.y;
			m.data[14] += position.z;
		}
		public static Matrix4 Scale(Vector3 scale) {
			double[] data = new double[0x10];
			data[0] = scale.x;
			data[5] = scale.y;
			data[10] = scale.z;
			data[15] = 1.0;
			return new Matrix4(data);
		}
		public static Matrix4 Translate(Vector3 position) {
			double[] data = new double[0x10];
			data[0] = 1.0;
			data[5] = 1.0;
			data[10] = 1.0;
			data[12] = position.x;
			data[13] = position.y;
			data[14] = position.z;
			data[15] = 1.0;
			return new Matrix4(data);
		}
		public static Matrix4 Rotate(Quaternion rotation) {
			double[] numArray = new double[0x10];
			numArray[0] = 1.0 - 2.0 * rotation.y * rotation.y - 2.0 * rotation.z * rotation.z;
			numArray[1] = 2.0 * rotation.x * rotation.y + 2.0 * rotation.z * rotation.w;
			numArray[2] = 2.0 * rotation.x * rotation.z - 2.0 * rotation.y * rotation.w;
			numArray[4] = 2.0 * rotation.x * rotation.y - 2.0 * rotation.z * rotation.w;
			numArray[5] = 1.0 - 2.0 * rotation.x * rotation.x - 2.0 * rotation.z * rotation.z;
			numArray[6] = 2.0 * rotation.y * rotation.z + 2.0 * rotation.x * rotation.w;
			numArray[8] = 2.0 * rotation.x * rotation.z + 2.0 * rotation.y * rotation.w;
			numArray[9] = 2.0 * rotation.y * rotation.z - 2.0 * rotation.x * rotation.w;
			numArray[10] = 1.0 - 2.0 * rotation.x * rotation.x - 2.0 * rotation.y * rotation.y;			
			numArray[15] = 1.0;
			return new Matrix4(numArray);
		}
		public static Matrix4 Rotate(double angle, Vector3 axis) {
			double num = MathS.Cos(-angle);
			double num2 = MathS.Sin(-angle);
			double num3 = 1.0 - num;
			double num4 = num2 * axis.x;
			double num5 = num2 * axis.y;
			double num6 = num2 * axis.z;
			double num7 = num3 * axis.x * axis.x;
			double num8 = num3 * axis.x * axis.y;
			double num9 = num3 * axis.x * axis.z;
			double num10 = num3 * axis.y * axis.y;
			double num11 = num3 * axis.y * axis.z;
			double num12 = num3 * axis.z * axis.z;
			double[] data = new double[0x10];
			data[0] = num7 + num;
			data[1] = num8 - num6;
			data[2] = num9 + num5;
			data[4] = num8 + num6;
			data[5] = num10 + num;
			data[6] = num11 - num4;
			data[8] = num9 - num5;
			data[9] = num11 + num4;
			data[10] = num12 + num;
			data[15] = 1.0;
			return new Matrix4(data);
		}
		public static Matrix4 LookAt(Vector3 from, Vector3 to, Vector3 up) {
			Vector3 b = Vector3.Normalize(from - to);
			Vector3 vector2 = Vector3.Normalize(Vector3.Cross(up, b));
			Vector3 a = Vector3.Normalize(Vector3.Cross(b, vector2));
			double[] data = new double[0x10];
			data[0] = vector2.x;
			data[1] = a.x;
			data[2] = b.x;
			data[4] = vector2.y;
			data[5] = a.y;
			data[6] = b.y;
			data[8] = vector2.z;
			data[9] = a.z;
			data[10] = b.z;
			data[12] = -Vector3.Dot(vector2, from);
			data[13] = -Vector3.Dot(a, from);
			data[14] = -Vector3.Dot(b, from);
			data[15] = 1.0;
			return new Matrix4(data);
		}
		public static Matrix4 Orthographic(double width, double height, double zNear, double zFar) {
			return Orthographic(-width / 2.0, width / 2.0, -height / 2.0, height / 2.0, zNear, zFar);
		}
		public static Matrix4 Orthographic(double left, double right, double bottom, double top, double zNear, double zFar) {
			double[] data = new double[0x10];
			data[0] = 2.0 / (right - left);
			data[5] = 2.0 / (top - bottom);
			data[10] = -2.0 / (zFar - zNear);
			data[12] = -(right + left) / (right - left);
			data[13] = -(top + bottom) / (top - bottom);
			data[14] = -(zFar + zNear) / (zFar - zNear);
			data[15] = 1.0;
			return new Matrix4(data);
		}
		public static Matrix4 Perspective(double fov, double aspect, double zNear, double zFar) {
			double top = zNear * MathS.Tan(0.5 * fov);
			return Perspective(-top * aspect, top * aspect, -top, top, zNear, zFar);
		}
		public static Matrix4 Perspective(double left, double right, double bottom, double top, double zNear, double zFar) {
			double num = (2.0 * zNear) / (right - left);
			double num2 = (2.0 * zNear) / (top - bottom);
			double num3 = (right + left) / (right - left);
			double num4 = (top + bottom) / (top - bottom);
			double num5 = -(zFar + zNear) / (zFar - zNear);
			double num6 = -(2.0 * zFar * zNear) / (zFar - zNear);
			double[] data = new double[0x10];
			data[0] = num;
			data[5] = num2;
			data[8] = num3;
			data[9] = num4;
			data[10] = num5;
			data[11] = -1.0;
			data[14] = num6;
			return new Matrix4(data);
		}
		#endregion FUNCTIONS
		#region OPERATORS
		public static Vector3 operator *(Matrix4 m, Vector3 b) {
			return new Vector3(
				m.data[0] * b.x + m.data[4] * b.y + m.data[8] * b.z + m.data[12],
				m.data[1] * b.x + m.data[5] * b.y + m.data[9] * b.z + m.data[13],
				m.data[2] * b.x + m.data[6] * b.y + m.data[10] * b.z + m.data[14]
			);
		}
		public static Matrix4 operator *(Matrix4 a, Matrix4 b) {
			return new Matrix4(
				new double[] {
					a.data[0] * b.data[0] + a.data[1] * b.data[4] + a.data[2] * b.data[8] + a.data[3] * b.data[12],
					a.data[0] * b.data[1] + a.data[1] * b.data[5] + a.data[2] * b.data[9] + a.data[3] * b.data[13],
					a.data[0] * b.data[2] + a.data[1] * b.data[6] + a.data[2] * b.data[10] + a.data[3] * b.data[14],
					a.data[0] * b.data[3] + a.data[1] * b.data[7] + a.data[2] * b.data[11] + a.data[3] * b.data[15],
					a.data[4] * b.data[0] + a.data[5] * b.data[4] + a.data[6] * b.data[8] + a.data[7] * b.data[12],
					a.data[4] * b.data[1] + a.data[5] * b.data[5] + a.data[6] * b.data[9] + a.data[7] * b.data[13],
					a.data[4] * b.data[2] + a.data[5] * b.data[6] + a.data[6] * b.data[10] + a.data[7] * b.data[14],
					a.data[4] * b.data[3] + a.data[5] * b.data[7] + a.data[6] * b.data[11] + a.data[7] * b.data[15],
					a.data[8] * b.data[0] + a.data[9] * b.data[4] + a.data[10] * b.data[8] + a.data[11] * b.data[12],
					a.data[8] * b.data[1] + a.data[9] * b.data[5] + a.data[10] * b.data[9] + a.data[11] * b.data[13],
					a.data[8] * b.data[2] + a.data[9] * b.data[6] + a.data[10] * b.data[10] + a.data[11] * b.data[14],
					a.data[8] * b.data[3] + a.data[9] * b.data[7] + a.data[10] * b.data[11] + a.data[11] * b.data[15],
					a.data[12] * b.data[0] + a.data[13] * b.data[4] + a.data[14] * b.data[8] + a.data[15] * b.data[12],
					a.data[12] * b.data[1] + a.data[13] * b.data[5] + a.data[14] * b.data[9] + a.data[15] * b.data[13],
					a.data[12] * b.data[2] + a.data[13] * b.data[6] + a.data[14] * b.data[10] + a.data[15] * b.data[14],
					a.data[12] * b.data[3] + a.data[13] * b.data[7] + a.data[14] * b.data[11] + a.data[15] * b.data[15]
				});
		}
		#endregion OPERATORS
		#region IMPLICIT
		public static implicit operator double[](Matrix4 m) {
			return m.data;
		}
		#endregion IMPLICIT
		public override string ToString() {
			return string.Format("{0:F1}, {1:F1}, {2:F1}, {3:F1}\n {4:F1}, {5:F1}, {6:F1}, {7:F1}\n {8:F1}, {9:F1}, {10:F1}, {11:F1}\n {12:F1}, {13:F1}, {14:F1}, {15:F1}",
			                     new object[] {
			                     	this.data[0], this.data[4], this.data[8], this.data[12],
			                     	this.data[1], this.data[5], this.data[9], this.data[13],
			                     	this.data[2], this.data[6], this.data[10], this.data[14],
			                     	this.data[3], this.data[7], this.data[11], this.data[15]
			                     });
		}
	}
}