﻿namespace Simplex {
	public struct Vector3 {
		#region DATA
		private double[] data;
		#endregion DATA
		#region FIELDS
		public static readonly Vector3 right = new Vector3(1.0, 0.0, 0.0);
		public static readonly Vector3 left = new Vector3(-1.0, 0.0, 0.0);
		public static readonly Vector3 up = new Vector3(0.0, 1.0, 0.0);
		public static readonly Vector3 down = new Vector3(0.0, -1.0, 0.0);
		public static readonly Vector3 forward = new Vector3(0.0, 0.0, 1.0);
		public static readonly Vector3 backward = new Vector3(0.0, 0.0, -1.0);
		public static readonly Vector3 zero = new Vector3(0.0, 0.0, 0.0);
		public static readonly Vector3 one = new Vector3(1.0, 1.0, 1.0);
		#endregion FIELDS
		#region ACCESSORS
		public double x {
			get { return this.data[0]; }
		}
		public double y {
			get { return this.data[1]; }
		}
		public double z {
			get { return this.data[2]; }
		}
		#endregion ACCESSORS
		#region CONSTRUCTORS
		public Vector3(double x, double y, double z) {
			this.data = new double[] { x, y, z };
		}
		public Vector3(double x, double y) {
			this.data = new double[] { x, y, 0.0 };
		}
		#endregion CONSTRUCTORS
		#region FUNCTIONS
		public double magnitude {
			get { return Magnitude(this); }
		}
		public double sqrMagnitude {
			get { return SqrMagnitude(this); }
		}
		public Vector3 normalized {
			get { return Normalize(this); }
		}
		public static double Magnitude(Vector3 v) {
			return MathS.Sqrt(SqrMagnitude(v));
		}
		public static double SqrMagnitude(Vector3 v) {
			return v.data[0] * v.data[0] + v.data[1] * v.data[1] + v.data[2] * v.data[2];
		}
		public static Vector3 Cross(Vector3 a, Vector3 b) {
			return new Vector3(a.data[1] * b.data[2] - a.data[2] * b.data[1], a.data[2] * b.data[0] - a.data[0] * b.data[2], a.data[0] * b.data[1] - a.data[1] * b.data[0]);
		}
		public static double Dot(Vector3 a, Vector3 b) {
			return a.data[0] * b.data[0] + a.data[1] * b.data[1] + a.data[2] * b.data[2];
		}
		public static Vector3 Normalize(Vector3 v) {
			double num = 1.0 / v.magnitude;
			return new Vector3(v.data[0] * num, v.data[1] * num, v.data[2] * num);
		}
		public static double Angle(Vector3 a, Vector3 b) {
			double num = Vector3.Dot(a.normalized, b.normalized);
			return MathS.Approximately(num, 1.0) ? 0.0 : MathS.Acos(MathS.Abs(num));
		}
		public static double Distance(Vector3 a, Vector3 b) {
			Vector3 vector = new Vector3(a.data[0] - b.data[0], a.data[1] - b.data[1], a.data[2] - b.data[2]);
			return vector.magnitude;
		}
		public static Vector3 Lerp(Vector3 a, Vector3 b, double t) {
			return new Vector3(MathS.Lerp(a.data[0], b.data[0], t), MathS.Lerp(a.data[1], b.data[1], t),MathS.Lerp(a.data[2], b.data[2], t));
		}
		#endregion FUNCTIONS
		#region OPERATORS
		public static Vector3 operator *(Vector3 v, double d) {
			return new Vector3(v.data[0] * d, v.data[1] * d, v.data[2] * d);
		}
		public static Vector3 operator *(double d, Vector3 v) {
			return new Vector3(v.data[0] * d, v.data[1] * d, v.data[2] * d);
		}
		public static Vector3 operator /(Vector3 v, double d) {
			double num = 1.0 / d;
			return new Vector3(v.data[0] * num, v.data[1] * num, v.data[2] * num);
		}
		public static Vector3 operator *(Vector3 a, Vector3 b) {
			return new Vector3(a.data[0] * b.data[0], a.data[1] * b.data[1], a.data[2] * b.data[2]);
		}
		public static Vector3 operator +(Vector3 a, Vector3 b) {
			return new Vector3(a.data[0] + b.data[0], a.data[1] + b.data[1], a.data[2] + b.data[2]);
		}
		public static Vector3 operator -(Vector3 a, Vector3 b) {
			return new Vector3(a.data[0] - b.data[0], a.data[1] - b.data[1], a.data[2] - b.data[2]);
		}
		public static Vector3 operator -(Vector3 v) {
			return new Vector3(-v.data[0], -v.data[1], -v.data[2]);
		}
		#endregion OPERATORS
		#region IMPLICIT
		public static implicit operator Vector2(Vector3 v) {
			return new Vector2(v.data[0], v.data[1]);
		}
		public static implicit operator double[](Vector3 v) {
			return v.data;
		}
		#endregion IMPLICIT
		public override string ToString() {
			return string.Format("{0:F1}, {1:F1}, {2:F1}", this.data[0], this.data[1], this.data[2]);
		}
	}
}