﻿namespace Simplex {
	public struct Vector2 {
		#region DATA
		private double[] data;
		#endregion DATA
		#region FIELDS
		public static readonly Vector2 right = new Vector2(1.0, 0.0);
		public static readonly Vector2 left = new Vector2(-1.0, 0.0);
		public static readonly Vector2 up = new Vector2(0.0, 1.0);
		public static readonly Vector2 down = new Vector2(0.0, -1.0);
		public static readonly Vector2 zero = new Vector2(0.0, 0.0);
		public static readonly Vector2 one = new Vector2(1.0, 1.0);
		#endregion FIELDS
		#region ACCESSORS
		public double x {
			get { return this.data[0]; }
		}
		public double y {
			get { return this.data[1]; }
		}
		#endregion ACCESSORS
		#region CONSTRUCTOR
		public Vector2(double x, double y) {
			this.data = new double[] { x, y };
		}
		#endregion CONSTRUCTOR
		#region FUNCTIONS
		public double magnitude {
			get { return Magnitude(this); }
		}
		public double sqrMagnitude {
			get { return SqrMagnitude(this); }
		}
		public Vector2 normalized {
			get { return Normalize(this); }
		}
		public static Vector2 Rotate90(Vector2 v) {
			return new Vector2(v.data[1], -v.data[0]);
		}
		public static double ToEuler(Vector2 direction) {
			return MathS.Atan2(direction.data[0], direction.data[1]);
		}
		public static Vector2 FromEuler(double angle) {
			return new Vector2(MathS.Sin(angle), MathS.Cos(angle));
		}
		public static double Magnitude(Vector2 v) {
			return MathS.Sqrt(SqrMagnitude(v));
		}
		public static double SqrMagnitude(Vector2 v) {
			return v.data[0] * v.data[0] + v.data[1] * v.data[1];
		}
		public static double Cross(Vector2 a, Vector2 b) {
			return a.data[0] * b.data[1] - a.data[1] * b.data[0];
		}
		public static double Dot(Vector2 a, Vector2 b) {
			return a.data[0] * b.data[0] + a.data[1] * b.data[1];
		}
		public static Vector2 Normalize(Vector2 v) {
			double num = 1.0 / v.magnitude;
			return new Vector2(v.data[0] * num, v.data[1] * num);
		}
		public static double Angle(Vector2 a, Vector2 b) {
			return ToEuler(a) - ToEuler(b);
		}
		public static double Distance(Vector2 a, Vector2 b) {
			Vector2 vector = new Vector2(a.data[0] - b.data[0], a.data[1] - b.data[1]);
			return vector.magnitude;
		}
		public static Vector2 Lerp(Vector2 a, Vector2 b, double t) {
			return new Vector2(MathS.Lerp(a.data[0], b.data[0], t), MathS.Lerp(a.data[1], b.data[1], t));
		}
		#endregion FUNCTIONS
		#region OPERATORS
		public static Vector2 operator *(Vector2 v, double d) {
			return new Vector2(v.data[0] * d, v.data[1] * d);
		}
		public static Vector2 operator *(double d, Vector2 v) {
			return new Vector2(v.data[0] * d, v.data[1] * d);
		}
		public static Vector2 operator /(Vector2 v, double d) {
			double num = 1.0 / d;
			return new Vector2(v.data[0] * num, v.data[1] * num);
		}
		public static Vector2 operator *(Vector2 a, Vector2 b) {
			return new Vector2(a.data[0] * b.data[0], a.data[1] * b.data[1]);
		}
		public static Vector2 operator +(Vector2 a, Vector2 b) {
			return new Vector2(a.data[0] + b.data[0], a.data[1] + b.data[1]);
		}
		public static Vector2 operator -(Vector2 a, Vector2 b) {
			return new Vector2(a.data[0] - b.data[0], a.data[1] - b.data[1]);
		}
		public static Vector2 operator -(Vector2 v) {
			return new Vector2(-v.data[0], -v.data[1]);
		}
		#endregion OPERATORS
		#region IMPLICIT
		public static implicit operator Vector3(Vector2 v) {
			return new Vector3(v.data[0], v.data[1], 0.0);
		}
		public static implicit operator double[](Vector2 v) {
			return v.data;
		}
		#endregion IMPLICIT
		public override string ToString() {
			return string.Format("{0:F1}, {1:F1}", this.data[0], this.data[1]);
		}
	}
}