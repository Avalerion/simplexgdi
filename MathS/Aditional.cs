﻿namespace Simplex {
	public static partial class MathS {
		#region FIELDS
		public const double Deg2Rad =  PI / 180.0;
		public const double Rad2Deg = 180.0 / PI;
		public static readonly double epsilon = CalculateEpsilon();
		#endregion FIELDS
		#region FUNCTIONS
		public static double Lerp(double from, double to, double t){
			return from + (to - from) * t;
		}
		public static double PingPong(double t, double length){
			return length - Abs(t % (length * 2.0) - length);
		}
		public static double Warp(double value, double min, double max){
			if (value >= min){
				return min + ((value - min) % (max - min));
			}
			return max - (min - value) % (max - min);
		}
		public static double Clamp(double value, double min, double max){
			if (value <= min){
				return min;
			} else if (value <= max){
				return value;
			}
			return max;
		}
		public static bool Approximately(double a, double b){
			return Abs(a - b) < epsilon;
		}
		private static double CalculateEpsilon(){
			double num = 1.0;
			while (1.0 + (num / 2.0) > 1.0){
				num /= 2.0;
			}
			return num;
		}
		#endregion FUNCTIONS
	}
}