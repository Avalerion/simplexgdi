﻿using System;
namespace Simplex {
	public static partial class MathS {
		#region FIELDS
		public const double PI = Math.PI;
		public const double E = Math.E;
		#endregion FIELDS
		#region FUNCTIONS
		public static double Abs(double value){
			return Math.Abs(value);
		}
		public static double Acos(double value){
			return Math.Acos(value);
		}
		public static double Asin(double value){
			return Math.Asin(value);
		}
		public static double Atan(double value){
			return Math.Atan(value);
		}
		public static double Atan2(double y, double x){
			return Math.Atan2(y, x);
		}
		public static double Ceiling(double value){
			return Math.Ceiling(value);
		}
		public static double Cos(double value){
			return Math.Cos(value);
		}
		public static double Exp(double value){
			return Math.Exp(value);
		}
		public static double Floor(double value){
			return Math.Floor(value);
		}
		public static double Log(double value){
			return Math.Log(value);
		}
		public static double Log(double value, double newBase){
			return Math.Log(value, newBase);
		}
		public static double Log10(double value){
			return Math.Log10(value);
		}
		public static double Max(double a, double b){
			return Math.Max(a, b);
		}
		public static double Min(double a, double b){
			return Math.Min(a, b);
		}
		public static double Round(double value){
			return Math.Round(value);
		}
		public static int Sign(double value){
			return Math.Sign(value);
		}
		public static double Sin(double value){
			return Math.Sin(value);
		}
		public static double Sqrt(double value){
			return Math.Sqrt(value);
		}
		public static double Tan(double value){
			return Math.Tan(value);
		}
		#endregion FUNCTIONS
	}
}